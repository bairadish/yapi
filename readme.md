#   项目说明
yapi中为项目代码, 基本没有修改

主要改动为:
1.  删掉了package.json中的githook
    1.  维护性提交不需要走规范
2.  删除日志写入部分
    1.  线上没有写入当前目录的权限, 因此需要注释掉
        1.  yapi/server/yapi.js
            1.  原代码
                1.  ```js
                    fs.ensureDirSync(WEBROOT_LOG);
                    ```
            2.  修改后
                1.  ```js
                    // 服务器上没权限, 注掉
                    // fs.ensureDirSync(WEBROOT_LOG);
                    ```
        2.  yapi/server/utils/common.js
            1.  修改一
                1.  原代码
                    1.  ```js
                        let logfile = path.join(yapi.WEBROOT_LOG, year + '-' + month + '.log');
                        ```
                2.  修改后
                    1.  ```js
                        // let logfile = path.join(yapi.WEBROOT_LOG, year + '-' + month + '.log');
                        ```
            2.  修改二
                1.  原代码
                    1.  ```js
                          fs.writeFileSync(logfile, data, {
                            flag: 'a'
                          });
                        ```
                2.  修改后
                    1.  ```js
                        //  fs.writeFileSync(logfile, data, {
                        //    flag: 'a'
                        //  });
                        ```
        3.  yapi/server/utils/initConfig.js
            1.  原代码
                1.  ```js
                    fs.ensureDirSync(path.join(runtimePath, 'log'));
                    ```
            2.  修改后
                1.  ```js
                    // fs.ensureDirSync(path.join(runtimePath, 'log'));
                    ```
        4.  yapi/app.js
            1.  添加以下代码, hack掉404页面, 以支持页面路由
                ```js
                // hack掉404页面, 所有非接口路由都交给前端进行处理
                app.use(async (ctx, next) => {
                const path = require('path');
                const fs = require('fs')
                let baseRoot = yapi.path.join(yapi.WEBROOT, 'static')
                let indexFileUri = path.resolve(baseRoot, indexFile)
                let content = fs.readFileSync(indexFileUri).toString()
                console.log('indexFileUri =>', indexFileUri)
                ctx.res.writeHead(200)
                ctx.res.write(content)
                ctx.res.end()
                await next()
                })

                ```
        5.  额外添加
            1.  `loader-utils`依赖, monaco-editor在webpack中需要该插件, 否则无法编译通过
3.  应该将贝壳扩展代码都提取到一个文件夹中, 方便查找/替换

升级方法

1.  下载最新的yapi代码
2.  将上述修改同步到新代码中
3.  直接删除yapi文件夹内的文件, 将原代码替换过来
初始化管理员账号成功,账号名："admin@admin.com"，密码："ymfe.org"