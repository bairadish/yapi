NAME := $(shell basename $(shell git config --get remote.origin.url) | sed 's/\.git//')

#FILENAME=$(NAME)-$(VERSION)-$(BRANCH).tar.gz
FILENAME=$(NAME).tar.gz
STATICNAME=yapi
TMPDIR=tmp
RELEASEDIR=release
YAPI=yapi
default: all

all: pack

test: init

init: clean

pack: 
	mkdir -p $(RELEASEDIR)
	mkdir -p $(TMPDIR)
	# todo(姚泽源) 临时注释掉build命令, 加速部署, 方便debug
	(cd ${YAPI} && npm install --registry https://registry.npm.taobao.org && npm run build-client)
	cp -R Makefile  bin  config.json  dev_config_demo.json yapi $(RELEASEDIR) # 把所有需要备份的文件都放在一个文件夹里, 统一压缩
	tar czf $(TMPDIR)/$(YAPI).tar.gz  $(RELEASEDIR)
clean:
	rm -rf $(TMPDIR)
	rm -rf $(RELEASEDIR)

.DEFAULT: all